sdpb-haskell
---------------

Haskell library for setting up bootstrap problems and running [SDPB](https://github.com/davidsd/sdpb).

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/sdpb-haskell/)
