{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StaticPointers #-}

module SDPB.Params
  ( Params(..)
  , MaxRuntime(..)
  , SolutionPart(..)
  , defaultParams
  , writeSolutionToString
  , allSolutionParts
  ) where

import Data.Aeson                 (FromJSON, ToJSON)
import Data.Binary                (Binary)
import Data.Binary.Instances.Time ()
import Data.List                  (intercalate)
import Data.Scientific            (Scientific)
import Data.Set                   qualified as Set
import Data.Time.Clock            (NominalDiffTime, UTCTime)
import GHC.Generics               (Generic)
import Hyperion                   (Dict (..), Static (..))

data Params = Params
  { precision                    :: Int
  , checkpointInterval           :: NominalDiffTime
  , noFinalCheckpoint            :: Bool
  , writeSolution                :: Set.Set SolutionPart
  , findPrimalFeasible           :: Bool
  , findDualFeasible             :: Bool
  , detectPrimalFeasibleJump     :: Bool
  , detectDualFeasibleJump       :: Bool
  , verbosity                    :: Int
  , maxIterations                :: Int
  , maxRuntime                   :: MaxRuntime
  , dualityGapThreshold          :: Scientific
  , primalErrorThreshold         :: Scientific
  , dualErrorThreshold           :: Scientific
  , initialMatrixScalePrimal     :: Scientific
  , initialMatrixScaleDual       :: Scientific
  , feasibleCenteringParameter   :: Scientific
  , infeasibleCenteringParameter :: Scientific
  , stepLengthReduction          :: Scientific
  , maxComplementarity           :: Scientific
  , minPrimalStep                :: Scientific
  , minDualStep                  :: Scientific
  , maxSharedMemory              :: Maybe String
  } deriving (Show, Generic, Eq, Ord, Binary, ToJSON, FromJSON)

instance Static (Binary Params) where closureDict = static Dict

data MaxRuntime
  = RunForDuration NominalDiffTime
  | TerminateByTime UTCTime
  deriving (Show, Generic, Eq, Ord, Binary, ToJSON, FromJSON)

data SolutionPart
  = DualVector_y
  | DualMatrix_Y
  | PrimalVector_x
  | PrimalMatrix_X
  | Functional_z
  deriving (Show, Enum, Bounded, Generic, Eq, Ord, Binary, ToJSON, FromJSON)

allSolutionParts :: Set.Set SolutionPart
allSolutionParts = Set.fromList [minBound .. maxBound]

solutionPartToString :: SolutionPart -> String
solutionPartToString p = case p of
  DualVector_y   -> "y"
  DualMatrix_Y   -> "Y"
  PrimalVector_x -> "x"
  PrimalMatrix_X -> "X"
  Functional_z   -> "z"

writeSolutionToString :: Set.Set SolutionPart -> String
writeSolutionToString solnParts = intercalate "," $
  map solutionPartToString (Set.toList solnParts)

defaultParams :: Params
defaultParams = Params
  { precision                    = 400
  , checkpointInterval           = 3600
  , noFinalCheckpoint            = False
  , writeSolution                = Set.singleton Functional_z
  , findPrimalFeasible           = False
  , findDualFeasible             = False
  , detectPrimalFeasibleJump     = False
  , detectDualFeasibleJump       = False
  , verbosity                    = 1
  , maxIterations                = 1000
  , maxRuntime                   = RunForDuration (20 * 3600)
  , dualityGapThreshold          = 1e-30
  , primalErrorThreshold         = 1e-30
  , dualErrorThreshold           = 1e-30
  , initialMatrixScalePrimal     = 1e20
  , initialMatrixScaleDual       = 1e20
  , feasibleCenteringParameter   = 0.1
  , infeasibleCenteringParameter = 0.3
  , stepLengthReduction          = 0.7
  , maxComplementarity           = 1e100
  , minPrimalStep                = 0
  , minDualStep                  = 0
  , maxSharedMemory              = Nothing
  }
