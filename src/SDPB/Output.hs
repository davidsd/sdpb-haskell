{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}

module SDPB.Output
  ( Output(..)
  , TerminateReason(..)
  , isFinished
  , isUnfinished
  , isDualFeasible
  , isPrimalFeasible
  , readOutFile
  , readFunctional
  , readParse
  , vectorParser
  ) where

import Control.Applicative        ((<|>))
import Control.Monad.Catch        (Exception, throwM)
import Data.Aeson                 hiding (Result)
import Data.Attoparsec.Text       hiding (IResult (..))
import Data.Binary                (Binary)
import Data.Binary.Instances.Time ()
import Data.Scientific            (Scientific, toRealFloat)
import Data.Text                  (Text)
import Data.Text                  qualified as T
import Data.Text.IO               qualified as T
import Data.Time.Clock            (NominalDiffTime)
import Data.Vector                (Vector)
import Data.Vector                qualified as V
import GHC.Generics               (Generic)
import Hyperion                   (Dict (..), Static (..))
import System.FilePath.Posix      ((</>))

data Output = Output
  { terminateReason :: TerminateReason
  , primalObjective :: Scientific
  , dualObjective   :: Scientific
  , dualityGap      :: Scientific
  , primalError     :: Scientific
  , dualError       :: Scientific
  , runtime         :: NominalDiffTime
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary Output) where closureDict = static Dict

data TerminateReason
  = PrimalDualOptimal
  | PrimalFeasible
  | DualFeasible
  | PrimalFeasibleJumpDetected
  | DualFeasibleJumpDetected
  | MaxIterationsExceeded
  | MaxRuntimeExceeded
  | MaxComplementarityExceeded
  | PrimalStepTooSmall
  | DualStepTooSmall
  | UpdateSDPs
  deriving (Eq, Ord, Read, Show, Generic, Binary, ToJSON, FromJSON)

data SDPBParseError = SDPBParseError String
  deriving (Show, Exception)

readTerminateReason :: Text -> Parser TerminateReason
readTerminateReason s = case s of
  "found primal-dual optimal solution"     -> pure PrimalDualOptimal
  "found primal feasible solution"         -> pure PrimalFeasible
  "found dual feasible solution"           -> pure DualFeasible
  "primal feasible jump detected"          -> pure PrimalFeasibleJumpDetected
  "dual feasible jump detected"            -> pure DualFeasibleJumpDetected
  "maxIterations exceeded"                 -> pure MaxIterationsExceeded
  "maxRuntime exceeded"                    -> pure MaxRuntimeExceeded
  "maxComplementarity exceeded"            -> pure MaxComplementarityExceeded
  "primal step too small"                  -> pure PrimalStepTooSmall
  "dual step too small"                    -> pure DualStepTooSmall
  "met criteria to update sdp input files" -> pure UpdateSDPs
  _                                        -> fail ("unrecognized terminate reason:" ++ T.unpack s)

isDualFeasible :: Output -> Bool
isDualFeasible o =
  terminateReason o `elem` [DualFeasible, PrimalDualOptimal, DualFeasibleJumpDetected]

isPrimalFeasible :: Output -> Bool
isPrimalFeasible o =
  terminateReason o `elem` [PrimalFeasible, PrimalFeasibleJumpDetected]

isUnfinished :: Output -> Bool
isUnfinished o =
  terminateReason o `elem` [MaxRuntimeExceeded, MaxIterationsExceeded]

isFinished :: Output -> Bool
isFinished = not . isUnfinished

sdpbOutputParser :: Parser Output
sdpbOutputParser = Output
  <$> val "terminateReason" (readTerminateReason =<< quotedText)
  <*> val "primalObjective" scientific
  <*> val "dualObjective"   scientific
  <*> val "dualityGap"      scientific
  <*> val "primalError"     scientific
  <*> val "dualError"       scientific
  <*> val ("runtime" <|> "Solver runtime") (fmap realToFrac double)
  where
    val :: Parser Text -> Parser a -> Parser a
    val k v = skipSpace >> k >> skipSpace >> "=" >> skipSpace >> v <* ";"
    quotedText = "\"" *> takeWhile1 (/= '"') <* "\""

vectorParser :: RealFloat a => Parser (Vector a)
vectorParser = do
  height <- decimal
  _ <- skipSpace >> decimal @Int -- ignore "width"
  V.replicateM height (endOfLine >> fmap toRealFloat scientific)

readParse :: Parser a -> FilePath -> IO a
readParse p f = do
  contents <- T.readFile f
  case parseOnly p contents of
    Left e  -> throwM (SDPBParseError e)
    Right a -> return a

readOutFile :: FilePath -> IO Output
readOutFile outDir = readParse sdpbOutputParser (outDir </> "out.txt")

readFunctional :: RealFloat a => FilePath -> IO (Vector a)
readFunctional outDir = readParse vectorParser (outDir </> "z.txt")
