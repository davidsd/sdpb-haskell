{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE ApplicativeDo       #-}
{-# LANGUAGE BlockArguments      #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE NoFieldSelectors    #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeOperators       #-}

module SDPB.Write
  ( writeSDP
  , SDPPart(..)
  , PartType(..)
  , PartChunk
  , Part
  , parts
  , partDependencies
  , partPath
  , partChunkPath
  , encodePart
  , encodePartChunk
  , withPartChunk
  , BigFloat
  ) where

import Bootstrap.Math.DampedRational (DampedRational (..), One)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.Polynomial     qualified as Pol
import Control.DeepSeq               (NFData, deepseq)
import Control.Monad                 (forM, join, replicateM)
import Control.Monad.Extra           (unlessM)
import Control.Monad.IO.Class        (MonadIO, liftIO)
import Data.Aeson                    (FromJSON, ToJSON, Value, pairs,
                                      toEncoding, toJSON, (.=))
import Data.Aeson                    qualified as Aeson
import Data.Aeson.Encoding           qualified as E
import Data.Binary                   (Binary)
import Data.Binary                   qualified as Binary
import Data.Functor.Compose          (Compose (..))
import Data.Functor.Identity         (Identity (..))
import Data.List                     (intercalate)
import Data.Matrix                   (Matrix)
import Data.Matrix                   qualified as MU
import Data.MultiSet                 (MultiSet)
import Data.MultiSet                 qualified as MultiSet
import Data.Scientific               qualified as S
import Data.Text                     qualified as Text
import Data.Vector                   (Vector)
import Data.Vector                   qualified as V
import GHC.Generics                  (Generic)
import GHC.TypeNats                  (KnownNat)
import Hyperion                      (Dict (..), Static (..), cAp)
import Hyperion.Log                  qualified as Log
import Numeric.Rounded               (Precision, Rounded, Rounding,
                                      RoundingMode (..))
import SDPB.SDP                      (Label (..), MaybeMonadic (..),
                                      NormalizationChunk (..),
                                      ObjectiveChunk (..),
                                      PositiveConstraintChunk (..),
                                      PositiveMatrix (..), SDP (..))
import SDPB.SDP                      qualified as SDP
import System.Directory              (createDirectoryIfMissing, doesPathExist,
                                      renameFile)
import System.FilePath.Posix         (takeDirectory, (<.>), (</>))
import System.Random                 (randomRIO)
import Type.Reflection               (Typeable)

type BigFloat p = Rounded 'TowardZero p

type SDPDirectory = FilePath

undefined_toJSON :: a -> Value
undefined_toJSON = error "This type only implements toEncoding"

newtype RoundedString r p = RoundedString (Rounded r p)

instance (Rounding r, Precision p) => ToJSON (RoundedString r p) where
  toJSON = undefined_toJSON
  toEncoding (RoundedString r) = E.scientificText (S.fromFloatDigits r)

runMaybeIdentity :: MaybeMonadic Identity a -> a
runMaybeIdentity = \case
  Pure x -> x
  Monadic (Identity x) -> x

instance (Rounding r, Precision p) => ToJSON (NormalizationChunk Identity (Rounded r p)) where
  toJSON          = undefined_toJSON
  toEncoding norm = pairs ("normalization" .= fmap RoundedString (runMaybeIdentity norm.vector))

instance (Rounding r, Precision p) => ToJSON (ObjectiveChunk Identity (Rounded r p)) where
  toJSON         = undefined_toJSON
  toEncoding obj = pairs ("objective" .= fmap RoundedString (runMaybeIdentity obj.vector))

data PrefactorJSON a = PrefactorJSON
  { base  :: a
  , poles :: MultiSet Rational
  }

instance (Rounding r, Precision p) => ToJSON (PrefactorJSON (Rounded r p)) where
  toJSON = undefined_toJSON
  toEncoding pre = pairs
    (  "base"     .= RoundedString pre.base
    <> "constant" .= RoundedString @r @p 1
    <> "poles"    .= fmap (RoundedString @r @p . fromRational) (MultiSet.toList pre.poles)
    )

newtype PolynomialJSON r p = PolynomialJSON (Pol.Polynomial (Rounded r p))

-- | Note: We use 'Pol.chopCoefficients' to remove the leading
-- coefficients of the polynomial less than sqrt epsilon, where
-- epsilon is defined in Numeric.Limits. This is useful when it's
-- possible for cancellations between polynomials to result in very
-- small coefficients due to floating point errors. Such small
-- coefficients could cause inefficiencies (or worse, instabilities)
-- in the solver.
instance (Rounding r, Precision p) => ToJSON (PolynomialJSON r p) where
  toJSON = undefined_toJSON
  toEncoding (PolynomialJSON p) =
    toEncoding . fmap RoundedString . Pol.paddedCoefficients . Pol.chopCoefficients $ p

instance (Rounding r, Precision p) => ToJSON (PositiveMatrix (Rounded r p)) where
  toJSON = undefined_toJSON
  toEncoding mat  = pairs
    (  "prefactor"   .= PrefactorJSON mat.base mat.poles
    <> "polynomials" .= MU.toLists (fmap (fmap PolynomialJSON) mat.matrix)
    <> case mat.numPoles of
         Nothing -> mempty
         Just nPoles -> "reducedPrefactor" .= PrefactorJSON mat.base (takePoles nPoles mat.poles)
    )

-- | Take the 'nPoles' right-most poles. This includes multiplicity,
-- so for example if we had poles at -3,-2,-1, with multiplicities
-- 5,10,6, and 'nPoles = 7', then we would end up with poles at -2,-1
-- with multiplicities 1,6.
takePoles :: Int -> MultiSet Rational -> MultiSet Rational
takePoles nPoles poles =
  MultiSet.fromList .
  take nPoles .
  reverse $
  MultiSet.toAscList poles

instance (Rounding r, Precision p) => ToJSON (PositiveConstraintChunk Identity (Rounded r p)) where
  toJSON = undefined_toJSON
  toEncoding ms = pairs ("PositiveMatrixWithPrefactorArray" .= runMaybeIdentity ms.matrices)

-- | Part of an SDP. We allow the vectors in the SDP to be broken into
-- chunks. When 'i ~ Int', then chunkIndex represents an index for
-- those chunks. When we want to represent the whole vector, we choose
-- 'i ~ ()'.
data SDPPart i = SDPPart
  { ty         :: PartType
  , label      :: Label
  , chunkIndex :: i
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

type Part      = SDPPart ()
type PartChunk = SDPPart Int

instance (Typeable i, Static (Binary i)) => Static (Binary (SDPPart i)) where
  closureDict = static (\Dict -> Dict) `cAp` closureDict @(Binary i)

data PartType = SDPObjective | SDPNormalization | SDPPositiveConstraint Int
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

partTypeString :: PartType -> String
partTypeString = \case
  SDPObjective            -> "obj"
  SDPNormalization        -> "norm"
  SDPPositiveConstraint _ -> "constraint"

concatNonEmpty :: [String] -> String
concatNonEmpty = intercalate "_" . filter (/= "")

-- | The path corresponding to a PartChunk. Note that we redundantly
-- prepend the partDir and chunkDir to the name of the file, so that
-- the name of the file itself is meaningful.
partChunkPath :: SDPDirectory -> PartChunk -> FilePath
partChunkPath dir partChunk =
  dir </> partDir </> chunkDir </> partChunkName <.> "dat"
  where
    partDir       = concatNonEmpty [partTypeString partChunk.ty, Text.unpack partChunk.label.name]
    chunkDir      = concatNonEmpty ["chunk", show partChunk.chunkIndex]
    partChunkName = concatNonEmpty [partDir, chunkDir, Text.unpack partChunk.label.hash]

-- | The path corresponding to a Part. Note that we redundantly
-- prepend the partDir to the name of the file, so that the name of
-- the file itself is meaningful.
partPath :: SDPDirectory -> Part -> FilePath
partPath dir part =
  dir </> partDir </> partName <.> "json"
  where
    partDir  = concatNonEmpty [partTypeString part.ty, Text.unpack part.label.name]
    partName = concatNonEmpty [partDir, Text.unpack part.label.hash]

assumeMonadic :: String -> MaybeMonadic m a -> m a
assumeMonadic fnName = \case
  Pure _ -> error (fnName <> ": expected 'Monadic' but found 'Pure'")
  Monadic m -> m

isMonadic :: MaybeMonadic m a -> Bool
isMonadic = \case
  Pure _ -> False
  Monadic _ -> True

-- | Apply 'f' to the given partChunk of the SDP (discarding the label).
withPartChunk
  :: SDP m a
  -> (forall c. m c -> b)
  -> PartChunk
  -> b
withPartChunk sdp f partChunk = case partChunk.ty of
  SDPObjective ->
    f . assumeMonadic "withPartChunk" $
    (sdp.objective.chunks V.! partChunk.chunkIndex).vector
  SDPNormalization ->
    f . assumeMonadic "withPartChunk" $
    (sdp.normalization.chunks V.! partChunk.chunkIndex).vector
  SDPPositiveConstraint k ->
    f . assumeMonadic "withPartChunk" $
    ((sdp.constraints !! k).chunks V.! partChunk.chunkIndex).matrices

-- | Return an IO action that encodes the given SDPPart to a file. We
-- do not perform the action here in order to allow static analysis of
-- the Applicative computation leading to this action, like computing
-- dependencies.
encodePartChunk
  :: (Applicative f, KnownNat p)
  => SDPDirectory
  -> PartChunk
  -> SDP f (BigFloat p)
  -> f (IO ())
encodePartChunk dir partChunk sdp =
  let path = partChunkPath dir partChunk
  in case partChunk.ty of
    SDPObjective ->
      fmap (encodeBinaryFileAtomic path) $
      assumeMonadic "withPartChunk" $
      (sdp.objective.chunks V.! partChunk.chunkIndex).vector
    SDPNormalization ->
      fmap (encodeBinaryFileAtomic path) $
      assumeMonadic "withPartChunk" $
      (sdp.normalization.chunks V.! partChunk.chunkIndex).vector
    SDPPositiveConstraint k ->
      fmap (encodeBinaryFileAtomic path) $
      assumeMonadic "withPartChunk" $
      ((sdp.constraints !! k).chunks V.! partChunk.chunkIndex).matrices

parts :: SDP m a -> [Part]
parts sdp =
  [ SDPPart SDPObjective sdp.objective.label ()
  , SDPPart SDPNormalization sdp.normalization.label ()
  ] ++
  [ SDPPart (SDPPositiveConstraint k) constraint.label ()
  | (k, constraint) <- zip [0..] sdp.constraints
  ]

objectivePartChunks :: SDP m a -> Vector (PartChunk, ObjectiveChunk m a)
objectivePartChunks sdp = do
  (i, chunk) <- V.indexed sdp.objective.chunks
  pure (SDPPart SDPObjective chunk.label i, chunk)

normalizationPartChunks :: SDP m a -> Vector (PartChunk, NormalizationChunk m a)
normalizationPartChunks sdp = do
  (i, chunk) <- V.indexed sdp.normalization.chunks
  pure (SDPPart SDPNormalization chunk.label i, chunk)

constraintPartChunks :: Int -> SDP m a -> Vector (PartChunk, PositiveConstraintChunk m a)
constraintPartChunks k sdp = do
  (i, chunk) <- V.indexed (sdp.constraints !! k).chunks
  pure (SDPPart (SDPPositiveConstraint k) chunk.label i, chunk)

-- | We define a "dependency" of a Part as a Monadic PartChunk, which
-- we will pre-compute and save to a file. Pure PartChunk's are
-- computed on the fly in 'encodePart'.
partDependencies :: SDP m a -> Part -> [PartChunk]
partDependencies sdp part = V.toList $ case part.ty of
  SDPObjective -> do
    (pc, c) <- objectivePartChunks sdp
    if isMonadic c.vector
      then pure pc
      else mempty
  SDPNormalization -> do
    (pc, c) <- normalizationPartChunks sdp
    if isMonadic c.vector
      then pure pc
      else mempty
  SDPPositiveConstraint k -> do
    (pc, c) <- constraintPartChunks k sdp
    if isMonadic c.matrices
      then pure pc
      else mempty

encodePart
  :: KnownNat p
  => SDPDirectory
  -> Part
  -> SDP f (BigFloat p)
  -> IO ()
encodePart dir part sdp =
  let path = partPath dir part
  in case part.ty of
    SDPObjective -> do
      Log.text "Reading objective from chunks"
      obj <- readObjective dir sdp
      Log.info "Writing objective to file" path
      encodeJsonFileAtomic path obj
    SDPNormalization -> do
      Log.text "Reading normalization from chunks"
      norm <- readNormalization dir sdp
      Log.info "Writing normalization to file" path
      encodeJsonFileAtomic path norm
    SDPPositiveConstraint k -> do
      Log.info "Reading constraint from chunks" k
      constraint <- readConstraint dir k sdp
      Log.info "Writing constraint to file" path
      encodeJsonFileAtomic path constraint

-- | Read PartChunk from a file and force the result with
-- 'deepseq'. Note: this function is unsafe. It will fail if called
-- with the wrong return type.
readPartChunkUnsafe :: (Binary a, NFData a) => SDPDirectory -> PartChunk -> IO a
readPartChunkUnsafe dir partChunk = do
  let path = partChunkPath dir partChunk
  Log.info "Reading PartChunk file" (partChunk, path)
  chunk <- Binary.decodeFile path
  chunk `deepseq` pure chunk

-- | When reading the objective, we loop over its PartChunk's. 'Pure'
-- chunks are computed inline -- we assume they are cheap to
-- compute. If we encounter a 'Monadic' PartChunk, we fetch it from
-- the appropriate file. The procedure for 'readNormalization' and
-- 'readConstraint' is similar.
readObjective
  :: (Binary a, NFData a)
  => SDPDirectory
  -> SDP m a
  -> IO (ObjectiveChunk Identity a)
readObjective dir sdp = do
  chunks <- forM (V.toList (objectivePartChunks sdp)) $ \(partChunk, chunk) ->
    case chunk.vector of
      Pure v -> pure v
      _      -> readPartChunkUnsafe dir partChunk
  pure $ MkObjectiveChunk (Pure (mconcat chunks)) sdp.objective.label

readNormalization
  :: (Binary a, NFData a)
  => SDPDirectory
  -> SDP m a
  -> IO (NormalizationChunk Identity a)
readNormalization dir sdp = do
  chunks <- forM (V.toList (normalizationPartChunks sdp)) $ \(partChunk, chunk) ->
    case chunk.vector of
      Pure v -> pure v
      _      -> readPartChunkUnsafe dir partChunk
  pure $ MkNormalizationChunk (Pure (mconcat chunks)) sdp.normalization.label

readConstraint
  :: (Binary a, NFData a, Eq a, Fractional a)
  => SDPDirectory
  -> Int
  -> SDP m a
  -> IO (PositiveConstraintChunk Identity a)
readConstraint dir k sdp = do
  chunks <- forM (V.toList (constraintPartChunks k sdp)) $ \(partChunk, chunk) ->
    case chunk.matrices of
      Pure mats -> pure mats
      _         -> readPartChunkUnsafe dir partChunk
  pure $ MkPositiveConstraintChunk (Pure (concatPositiveMatrices chunks)) (sdp.constraints !! k).label

-- | The vector represents different positive constraints. We must
-- assume that each of these vectors is the same size. We must also
-- assume that the list is non-empty.
concatPositiveMatrices :: (Eq a, Fractional a) => [Vector (PositiveMatrix a)] -> Vector (PositiveMatrix a)
concatPositiveMatrices mats =
  fmap concatPositiveMatrixList (transposeVectors mats)
  where
    concatPositiveMatrixList :: (Eq a, Fractional a) => [PositiveMatrix a] -> PositiveMatrix a
    concatPositiveMatrixList ms =
      let
        m1 = case ms of
          []  -> error "concatPositiveMatrices: empty list of matrices"
          m:_ -> m
        dr = DR.sequence (map positiveMatrixToDR ms)
      in
        MkPositiveMatrix
        { SDP.matrix   = fmap mconcat $
                         transposeMatrices $
                         fmap getCompose $
                         getCompose dr.numerator
        , SDP.poles    = dr.poles
        , SDP.base     = m1.base
        , SDP.numPoles = m1.numPoles
        }

    positiveMatrixToDR :: PositiveMatrix a -> DampedRational One (Matrix `Compose` Vector) a
    positiveMatrixToDR mat = DampedRational (Compose mat.matrix) mat.poles

    transposeVectors :: [Vector a] -> Vector [a]
    transposeVectors vs = case vs of
      [] -> error "Cannot transpose empty list of vectors"
      v : _ -> V.generate (V.length v) \j ->
        fmap (V.! j) vs

    transposeMatrices :: [Matrix a] -> Matrix [a]
    transposeMatrices ms = case ms of
      [] -> error "Cannot transpose empty list of matrices"
      m : _ -> MU.matrix (MU.nrows m) (MU.ncols m) \ij ->
        fmap (MU.! ij) ms

partChunksMonadic :: SDP m a -> [PartChunk]
partChunksMonadic sdp = parts sdp >>= partDependencies sdp

-- | 'IO' action that returns a random string of given length
randomString :: Int -> IO String
randomString len = replicateM len $ toAlpha <$> randomRIO (0, 51)
  where toAlpha n | n < 26    = toEnum (n + fromEnum 'A')
                  | otherwise = toEnum (n - 26 + fromEnum 'a')

-- | Write a json representation of 'x' to a temporary file, and then
-- move the temporary file into place.
encodeJsonFileAtomic :: ToJSON a => FilePath -> a -> IO ()
encodeJsonFileAtomic path x = do
  salt <- randomString 6
  let tmpPath = path <> "_" <> salt
  Log.info "Writing JSON file" path
  createDirectoryIfMissing True (takeDirectory path)
  Aeson.encodeFile tmpPath x
  renameFile tmpPath path

-- | Write a json representation of 'x' to a temporary file, and then
-- move the temporary file into place.
encodeBinaryFileAtomic :: Binary a => FilePath -> a -> IO ()
encodeBinaryFileAtomic path x = do
  salt <- randomString 6
  let tmpPath = path <> "_" <> salt
  Log.info "Writing Binary file" path
  createDirectoryIfMissing True (takeDirectory path)
  Binary.encodeFile tmpPath x
  renameFile tmpPath path

-- | Write JSON files sequentially. Files are not overwritten if they
-- already exist. TODO: Is this the correct default behavior?
writeSDP
  :: (MonadIO m, KnownNat p)
  => FilePath
  -> SDP IO (BigFloat p)
  -> m [FilePath]
writeSDP dir sdp = liftIO $ do
  createDirectoryIfMissing True dir
  _ <- sequence $ do
    partChunk <- partChunksMonadic sdp
    let partChunkFile = partChunkPath dir partChunk
    pure $ do
      unlessM (doesPathExist partChunkFile) $
        join (encodePartChunk dir partChunk sdp)
      return partChunkFile
  sequence $ do
    part <- parts sdp
    let partFile = partPath dir part
    pure $ do
      unlessM (doesPathExist partFile) $
        encodePart dir part sdp
      return partFile
