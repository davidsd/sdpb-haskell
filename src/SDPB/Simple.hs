{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE RecordWildCards #-}

-- | This module provides a simple interface to SDPB, suitable for
-- solving small SDPs that do not need to be written in parallel or
-- solved in a special way.

module SDPB.Simple
  ( SDPBConfig(..)
  , withSDPSolution
  ) where

import GHC.TypeNats          (KnownNat)
import SDPB.Input            (Input, defaultInput)
import SDPB.Output           (Output)
import SDPB.Params           (Params)
import SDPB.Run              (run)
import SDPB.SDP              (SDP)
import SDPB.Write            (BigFloat, writeSDP)
import System.Directory      (createDirectoryIfMissing)
import System.FilePath.Posix ((</>))
import System.IO.Temp        (withTempDirectory)


data SDPBConfig = SDPBConfig
  { sdpbParams        :: Params
  , pmp2sdpExecutable :: FilePath
  , sdpbExecutable    :: FilePath
  , workDir           :: FilePath
  } deriving (Show)

-- | Write the SDP to a temporary directory and solve it. Pass both
-- the 'SDPB.Input' and 'SDPB.Output' to the given continuation. The
-- directory is deleted as soon as we leave the scope of the
-- continuation.
withSDPSolution
  :: KnownNat p
  => SDPBConfig
  -> SDP IO (BigFloat p)
  -> ((Input, Output) -> IO a)
  -> IO a
withSDPSolution SDPBConfig{..} sdp f = do
  createDirectoryIfMissing True workDir
  withTempDirectory workDir "sdpb" $
    \baseDir -> do
      jsonFiles <- writeSDP (baseDir </> "json_dir") sdp
      let input = defaultInput (baseDir </> "sdp") jsonFiles sdpbParams
      output <- run pmp2sdpExecutable sdpbExecutable input
      f (input, output)
