{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module SDPB.Run
  ( run
  , runPmp2sdp
  , getParamArgs
  , makeNsvFile
  ) where

import Control.Monad.Catch    qualified as MC
import Control.Monad.Extra    (unlessM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.ByteString.Char8  qualified as BS
import Data.Time.Clock        (diffUTCTime, getCurrentTime)
import SDPB.Input             (Input (..))
import SDPB.Output            (Output, readOutFile)
import SDPB.Params            (MaxRuntime (..), Params (..),
                               writeSolutionToString)
import System.Directory       (doesPathExist)
import System.FilePath.Posix  (takeDirectory)
import System.IO              (Handle, hClose, hFlush,
                               openTempFileWithDefaultPermissions)
import System.Process         (callProcess)

-- | Create, open, and use a temporary file in the given directory.
--
-- The temp file is not deleted
withTempFileNoDelete
  :: (MonadIO m, MC.MonadMask m)
  => FilePath -- ^ Parent directory to create the file in
  -> String   -- ^ File name template
  -> (FilePath -> Handle -> m a) -- ^ Callback that can use the file
  -> m a
withTempFileNoDelete tmpDir template action =
  MC.bracket
  (liftIO (openTempFileWithDefaultPermissions tmpDir template))
  (\(_, handle) -> liftIO (hClose handle))
  (uncurry action)

getMaxRuntimeSeconds :: MaxRuntime -> IO Int
getMaxRuntimeSeconds = fmap floor . \case
  RunForDuration d  -> return d
  TerminateByTime t -> fmap (diffUTCTime t) getCurrentTime

-- | Make an NSV file containg the given list of FilePath's. The
-- directory is the same as the directory of the first path in the
-- list.
makeNsvFile :: [FilePath] -> IO FilePath
makeNsvFile paths =
  withTempFileNoDelete fileListDir "file_list.nsv" $ \nsvFile nsvHandle -> do
  mapM_ (BS.hPut nsvHandle) [BS.pack f <> "\0" | f <- paths]
  hFlush nsvHandle
  pure nsvFile
  where
    fileListDir = case paths of
      p : _ -> takeDirectory p
      []    -> error "Cannot make an NSV file for 0 files"

-- | Generate the sdp file needed by SDPB. Only runs if the file
-- doesn't already exist.
runPmp2sdp :: FilePath -> Input -> IO ()
runPmp2sdp pmp2sdpExecutable Input{ params = Params{..}, .. } = do
  unlessM (doesPathExist sdpDir) $ do
    nsvFile <- makeNsvFile jsonFiles
    callProcess pmp2sdpExecutable
      [ "--precision", show precision
      , "--input",     nsvFile
      , "--output",    sdpDir
      ]

-- | Get the list of arguments for the given 'Params'. This requires
-- IO because it computes maxRuntime using the current time.
getParamArgs :: Params -> IO [String]
getParamArgs Params{..} = do
  maxRunSecs <- getMaxRuntimeSeconds maxRuntime
  pure $
    [ "--precision",                    show precision
    , "--checkpointInterval",           show (floor checkpointInterval :: Int)
    , "--maxRuntime",                   show maxRunSecs
    , "--dualityGapThreshold",          show dualityGapThreshold
    , "--primalErrorThreshold",         show primalErrorThreshold
    , "--dualErrorThreshold",           show dualErrorThreshold
    , "--initialMatrixScalePrimal",     show initialMatrixScalePrimal
    , "--initialMatrixScaleDual",       show initialMatrixScaleDual
    , "--feasibleCenteringParameter",   show feasibleCenteringParameter
    , "--infeasibleCenteringParameter", show infeasibleCenteringParameter
    , "--stepLengthReduction",          show stepLengthReduction
    , "--maxComplementarity",           show maxComplementarity
    , "--maxIterations",                show maxIterations
    , "--verbosity",                    show verbosity
    , "--writeSolution",                writeSolutionToString writeSolution
    ] ++
    [ "--findPrimalFeasible"          | findPrimalFeasible]       ++
    [ "--findDualFeasible"            | findDualFeasible]         ++
    [ "--detectPrimalFeasibleJump"    | detectPrimalFeasibleJump] ++
    [ "--detectDualFeasibleJump"      | detectDualFeasibleJump] ++
    [ "--noFinalCheckpoint"           | noFinalCheckpoint]
      ++
    case maxSharedMemory of
      Just maxMem -> ["--maxSharedMemory", maxMem]
      Nothing     -> []


-- | Get the list of arguments for the given input.
getInputArgs :: Input -> IO [String]
getInputArgs Input{..} = do
  paramArgs <- getParamArgs params
  pure $
    paramArgs ++
    [ "--sdpDir",        sdpDir
    , "--outDir",        outDir
    , "--checkpointDir", checkpointDir
    ] ++
    case initialCheckpointDir of
      Just dir -> ["--initialCheckpointDir", dir]
      Nothing  -> []

-- | Run pmp2sdp and sdpb on the given 'Input' and read the results
-- from 'outDir'
run :: FilePath -> FilePath -> Input -> IO Output
run pmp2sdpExecutable sdpbExecutable input = do
  runPmp2sdp pmp2sdpExecutable input
  args <- getInputArgs input
  callProcess sdpbExecutable args
  readOutFile (outDir input)
