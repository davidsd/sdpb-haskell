{-# LANGUAGE DeriveAnyClass  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StaticPointers  #-}

module SDPB.Input
  ( Input(..)
  , defaultInput
  ) where

import Data.Aeson   (FromJSON, ToJSON)
import Data.Binary  (Binary)
import GHC.Generics (Generic)
import Hyperion     (Dict (..), Static (..))
import SDPB.Params  (Params)

data Input = Input
  { params               :: Params
  , jsonFiles            :: [FilePath]
  , sdpDir               :: FilePath
  , outDir               :: FilePath
  , checkpointDir        :: FilePath
  , initialCheckpointDir :: Maybe FilePath
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary Input) where closureDict = static Dict

defaultInput :: FilePath -> [FilePath] -> Params -> Input
defaultInput baseName jsonFiles params = Input
  { sdpDir                = baseName ++ "_sdp"
  , outDir                = baseName ++ "_out"
  , checkpointDir         = baseName ++ "_ck"
  , initialCheckpointDir  = Nothing
  , ..
  }
