{-# OPTIONS_GHC -fno-warn-ambiguous-fields #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module SDPB.SDP
  ( SDP(..)
  , PositiveMatrix(..)
  , mkPositiveMatrix
  , PositiveConstraintChunk(..)
  , PositiveConstraint(..)
  , ObjectiveChunk(..)
  , Objective(..)
  , NormalizationChunk(..)
  , Normalization(..)
  , MaybeMonadic(..)
  , singleConstraintChunk
  , singleConstraint
  , constantMatrix
  , HFunctor(..)
  , Label(..)
  , mkLabel
  ) where

import Bootstrap.Math.DampedRational (DampedRational)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.Linear         ()
import Bootstrap.Math.Polynomial     (Polynomial)
import Bootstrap.Math.VectorSpace    (Tensor)
import Control.DeepSeq               (NFData)
import Data.Aeson                    (FromJSON, ToJSON)
import Data.Binary                   (Binary)
import Data.BinaryHash               (hashBase64SafeText)
import Data.Functor.Compose          (Compose (..))
import Data.Matrix                   (Matrix)
import Data.Matrix.Static            qualified as MatrixStatic
import Data.MultiSet                 (MultiSet)
import Data.Reflection               (Reifies)
import Data.Text                     (Text)
import Data.Text                     qualified as Text
import Data.Vector                   (Vector)
import Data.Vector                   qualified as Vector
import GHC.Generics                  (Generic)
import Type.Reflection               (Typeable)


-- | A label for an SDP element. The hash should be unique. The name
-- does not need to be unique -- for example two different SDPs with
-- overlapping names can exist in the same directory, since their
-- files will be distinguished by hashes.
--
-- The 'name' and 'hash' should both be fewer than 200 characters.
data Label = MkLabel
  { name :: Text
  , hash :: Text
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

-- | Create a label with the given name, and a hash of the given
-- object.
mkLabel :: (Binary a, Typeable a) => Text -> a -> Label
mkLabel name x =
  if Text.length name > 200
  then error ("Label name cannot be longer than 200 characters: " <> Text.unpack name)
  else MkLabel name (hashBase64SafeText x)

-- | A matrix that we will demand be positive semidefinite
data PositiveMatrix a = MkPositiveMatrix
  { -- | A DampedRational valued in a matrix of vectors
    matrix   :: Matrix (Vector (Polynomial a))
  , poles    :: MultiSet Rational
  , base     :: a
    -- | An optional number of poles to use for "down-sampling" the
    -- constraint.
  , numPoles :: Maybe Int
  } deriving (Generic, Binary, NFData)

-- | Either a pure value of type 'a' or a monadic value of type 'm a'.
data MaybeMonadic m a = Pure a | Monadic (m a)

-- | A monadic/applicative action to compute a group of
-- PositiveMatrix's, together with a unique label. These matrices will
-- be computed together and stored in one file. NB: The name
-- 'PositiveConstraint' is for historical reasons and is a bit of a
-- misnomer because we can have multiple PositiveMatrix's.
data PositiveConstraintChunk m a =
  MkPositiveConstraintChunk
  { matrices :: MaybeMonadic m (Vector (PositiveMatrix a))
  , label    :: Label
  }

-- | A monadic/applicative action to compute the objective, together
-- with a unique label.
data ObjectiveChunk m a = MkObjectiveChunk
  { vector :: MaybeMonadic m (Vector a)
  , label  :: Label
  }

-- | A monadic/applicative action to compute the normalization,
-- together with a unique label.
data NormalizationChunk m a = MkNormalizationChunk
  { vector :: MaybeMonadic m (Vector a)
  , label  :: Label
  }

data PositiveConstraint m a = MkPositiveConstraint
  { chunks :: Vector (PositiveConstraintChunk m a)
  , label  :: Label
  }

data Objective m a = MkObjective
  { chunks :: Vector (ObjectiveChunk m a)
  , label  :: Label
  }

data Normalization m a = MkNormalization
  { chunks :: Vector (NormalizationChunk m a)
  , label  :: Label
  }

-- | A collection of monadic/applicative actions for computing the
-- parts of an SDP, together with labels for each.
data SDP m a = SDP
  { objective     :: Objective m a
  , normalization :: Normalization m a
  , constraints   :: [PositiveConstraint m a]
  }

mkPositiveMatrix
  :: Reifies base a
  => DampedRational base (Matrix `Tensor` Vector) a
  -> Maybe Int
  -> PositiveMatrix a
mkPositiveMatrix dr numPoles =
  MkPositiveMatrix (getCompose dr.numerator) dr.poles (DR.base dr) numPoles

-- | Construct a PositiveMatrix from a constant matrix (independent of x)
constantMatrix :: (Num a, Eq a) => MatrixStatic.Matrix j j (Vector a) -> PositiveMatrix a
constantMatrix mat =
  mkPositiveMatrix
  (DR.constant . Compose . MatrixStatic.unpackStatic $ mat)
  Nothing

-- | A 'PositiveConstraintChunk' consisting of a single matrix
singleConstraintChunk
  :: Functor m
  => Label
  -> m (PositiveMatrix a)
  -> PositiveConstraintChunk m a
singleConstraintChunk label cons =
  MkPositiveConstraintChunk (Monadic (fmap Vector.singleton cons)) label

-- | A 'PositiveConstraint' consisting of a single matrix
singleConstraint
  :: Functor m
  => Label
  -> m (PositiveMatrix a)
  -> PositiveConstraint m a
singleConstraint label cons = MkPositiveConstraint
  { chunks = Vector.singleton $ singleConstraintChunk label cons
  , label  = label
  }

-- | This code is from functor-combinators. We don't need the full
-- power of that package.

infixr 0 ~>
-- | A natural transformation from @f@ to @g@.
type f ~> g = forall x. f x -> g x

class HFunctor t where
  hmap :: (f ~> g) -> t f a -> t g a

instance HFunctor MaybeMonadic where
  hmap _ (Pure x)    = Pure x
  hmap f (Monadic m) = Monadic (f m)

instance HFunctor PositiveConstraintChunk where
  hmap f p = p { matrices = hmap f p.matrices }

instance HFunctor ObjectiveChunk where
  hmap f o = o { vector = hmap f o.vector }

instance HFunctor NormalizationChunk where
  hmap f n = n { vector = hmap f n.vector }

instance HFunctor PositiveConstraint where
  hmap f p = p { chunks = fmap (hmap f) p.chunks }

instance HFunctor Objective where
  hmap f o = o { chunks = fmap (hmap f) o.chunks }

instance HFunctor Normalization where
  hmap f n = n { chunks = fmap (hmap f) n.chunks }

-- | This instance allows us to transform the applicative/monad in
-- which the SDP is computed. For example, we can turn 'SDP (ReaderT r
-- IO) a' into 'SDP IO a'.
instance HFunctor SDP where
  hmap f s = SDP
    { objective = hmap f s.objective
    , normalization = hmap f s.normalization
    , constraints = fmap (hmap f) s.constraints
    }
